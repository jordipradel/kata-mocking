package edu.upc.fib.inlab.dojo.mocking.dal;

class ToDoInListRow implements Row {
    private String toDoId;
    private String listId;
    private int order;

    ToDoInListRow() {
    }

    ToDoInListRow(String toDoId, String listId, int order) {
        this.toDoId = toDoId;
        this.listId = listId;
        this.order = order;
    }

    public String getId() {
        return listId + "-" + order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDoInListRow that = (ToDoInListRow) o;

        if (order != that.order) return false;
        if (!listId.equals(that.listId)) return false;
        if (!toDoId.equals(that.toDoId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = toDoId.hashCode();
        result = 31 * result + listId.hashCode();
        result = 31 * result + order;
        return result;
    }

    public String getToDoId() {
        return toDoId;
    }

    public String getListId() {
        return listId;
    }

    public int getOrder() {
        return order;
    }

}
