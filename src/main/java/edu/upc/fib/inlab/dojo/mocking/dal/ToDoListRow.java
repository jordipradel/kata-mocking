package edu.upc.fib.inlab.dojo.mocking.dal;

import java.util.UUID;

public class ToDoListRow implements Row{
    private String id;
    private String name;

    public ToDoListRow() {
    }

    public ToDoListRow(String name) {
        if(name==null) throw new IllegalArgumentException("ToDoRow list name can't be null");
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getId() {
        return "" + id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ToDoListRow{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDoListRow that = (ToDoListRow) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        return result;
    }
}
