package edu.upc.fib.inlab.dojo.mocking.dal;

import java.io.Serializable;

public interface Row extends Serializable {
    public String getId();
}
