package edu.upc.fib.inlab.dojo.mocking.domain;

public class ToDo {
    private String id;
    private String title;
    private String description;
    private int priority;

    public ToDo(String title, String description, int priority) {
        this(null,title,description,priority);
    }

    public ToDo(String id, String title, String description, int priority) {
        if(title == null) throw new IllegalArgumentException("To Do title can't be null");
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }


    @Override
    public String toString() {
        return "ToDo{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDo toDo = (ToDo) o;

        if (priority != toDo.priority) return false;
        if (description != null ? !description.equals(toDo.description) : toDo.description != null) return false;
        if (id != null ? !id.equals(toDo.id) : toDo.id != null) return false;
        if (!title.equals(toDo.title)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + title.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + priority;
        return result;
    }
}
