package edu.upc.fib.inlab.dojo.mocking.domain;


import edu.upc.fib.inlab.dojo.mocking.Service;
import edu.upc.fib.inlab.dojo.mocking.dal.ToDoListRow;
import edu.upc.fib.inlab.dojo.mocking.dal.ToDoRepository;
import edu.upc.fib.inlab.dojo.mocking.dal.ToDoRow;

import java.util.LinkedList;
import java.util.List;

import static edu.upc.fib.inlab.dojo.mocking.ServiceLocator.locate;

public class ToDoController {

    public List<ToDoList> getToDoLists() {
        ToDoRepository repository = (ToDoRepository) locate(Service.toDoRepository);
        List<ToDoListRow> rows = repository.findAllLists("name");
        List<ToDoList> result = new LinkedList<ToDoList>();
        for (ToDoListRow r : rows) {
            List<ToDo> todos = getToDos(r.getId());
            result.add(new ToDoList(r.getId(), r.getName(), todos));
        }
        return result;
    }

    public List<ToDo> getToDos(String listId) {
        ToDoRepository repository = (ToDoRepository) locate(Service.toDoRepository);
        List<ToDoRow> rows = repository.findToDosByListId(listId);
        List<ToDo> result = new LinkedList<ToDo>();
        for (ToDoRow r : rows) {
            result.add(new ToDo(r.getId(), r.getTitle(), r.getDescription(), r.getPriority()));
        }
        return result;
    }

    public ToDoList getToDoList(String listName) {
        ToDoRepository repository = (ToDoRepository) locate(Service.toDoRepository);
        List<ToDoListRow> rows = repository.findAllLists("name");
        for (ToDoListRow r : rows) {
            if (r.getName().equals(listName)) {
                List<ToDo> todos = getToDos(r.getId());
                return new ToDoList(r.getId(), r.getName(), todos);
            }
        }
        return null;
    }

    public void newList(String listName) {
        ToDoRepository repository = (ToDoRepository) locate(Service.toDoRepository);
        repository.insertList(new ToDoListRow(listName));
    }
}
