package edu.upc.fib.inlab.dojo.mocking;

import edu.upc.fib.inlab.dojo.mocking.dal.ToDoRepository;
import edu.upc.fib.inlab.dojo.mocking.domain.ToDoController;



public final class ServiceLocator {

    private static final ToDoRepository TO_DO_REPOSITORY = new ToDoRepository();
    private static final ToDoController TO_DO_CONTROLLER = new ToDoController();

    public static Object locate(Service r){
        if(r == Service.toDoRepository) return TO_DO_REPOSITORY;
        if(r == Service.todoController) return TO_DO_CONTROLLER;
        throw new IllegalArgumentException();
    }
}
