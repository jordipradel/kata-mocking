package edu.upc.fib.inlab.dojo.mocking.dal;

import java.util.LinkedList;
import java.util.List;

public class ToDoRepository {

    private Table<ToDoRow> todos = new Table<ToDoRow>("todos");

    private Table<ToDoListRow> todoLists = new Table<ToDoListRow>("todo_lists");

    private Table<ToDoInListRow> todosInList = new Table<ToDoInListRow>("todos_in_lists");

    public ToDoRepository() {
        if (todos.findAll("id").isEmpty()) {
            // Simulate initial DB data
            ToDoRow red = new ToDoRow("Write a test", null, 10);
            ToDoRow green = new ToDoRow("Write the minimum code that makes it possibly work", null, 10);
            ToDoRow refactor = new ToDoRow("Refactor the solution", null, 10);
            ToDoRow indent = new ToDoRow("Indent code", "Indent according to team rules", 7);
            ToDoListRow tdd = new ToDoListRow("TDD");
            todos.insert(red);
            todos.insert(green);
            todos.insert(refactor);
            todos.insert(indent);
            todoLists.insert(tdd);
            todosInList.insert(new ToDoInListRow(red.getId(), tdd.getId(), 1));
            todosInList.insert(new ToDoInListRow(green.getId(), tdd.getId(), 2));
            todosInList.insert(new ToDoInListRow(refactor.getId(), tdd.getId(), 3));
            todosInList.insert(new ToDoInListRow(indent.getId(), tdd.getId(), 4));
        }else{
            System.out.println("Already found " + todos.findAll("id").size() + " todos");
        }
    }

    public int insertList(ToDoListRow row) {
        return todoLists.insert(row);
    }

    public int deleteList(String id) {
        return todoLists.delete(id);
    }

    public int updateList(String id, ToDoListRow row) {
        return todoLists.update(id, row);
    }

    public List<ToDoListRow> findAllLists(String orderBy) {
        return todoLists.findAll(orderBy);
    }

    public ToDoListRow findListById(String id) {
        return todoLists.findById(id);
    }

    public int insertToDo(ToDoRow row) {
        return todos.insert(row);
    }

    public int deleteToDo(String id) {
        return todos.delete(id);
    }

    public int updateToDo(String id, ToDoRow row) {
        return todos.update(id, row);
    }

    public List<ToDoRow> findAllToDos(String orderBy) {
        return todos.findAll(orderBy);
    }

    public ToDoRow findToDoById(String id) {
        return todos.findById(id);
    }

    public List<ToDoRow> findToDosByListId(String listId) {
        ToDoListRow list = todoLists.findById(listId);
        if (list == null) return new LinkedList<ToDoRow>();
        List<ToDoInListRow> relations = todosInList.findWhere("listId", listId, "order");
        List<ToDoRow> result = new LinkedList<ToDoRow>();
        for (ToDoInListRow r : relations) {
            result.add(todos.findById(r.getToDoId()));
        }
        return result;
    }

}
