package edu.upc.fib.inlab.dojo.mocking.domain;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ToDoList {
    private String id;
    private String name;
    private List<ToDo> todos = new LinkedList<ToDo>();

    public ToDoList(String name, List<ToDo> todos) {
        this(null,name,todos);
    }

    public ToDoList(String id, String name, List<ToDo> todos) {
        if(name==null) throw new IllegalArgumentException("To Do list name can't be null");
        if(todos == null) todos = new LinkedList<ToDo>();
        this.id = id;
        this.name = name;
        this.todos = todos;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<ToDo> getTodos() {
        return Collections.unmodifiableList(todos);
    }

    @Override
    public String toString() {
        return "ToDoList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", todos=" + todos +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDoList toDoList = (ToDoList) o;

        if (id != null ? !id.equals(toDoList.id) : toDoList.id != null) return false;
        if (!name.equals(toDoList.name)) return false;
        if (!todos.equals(toDoList.todos)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + todos.hashCode();
        return result;
    }
}
