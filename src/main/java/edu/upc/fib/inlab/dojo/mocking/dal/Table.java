package edu.upc.fib.inlab.dojo.mocking.dal;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Table<T extends Row> {

    private String tableName;
    private List<T> rows = new LinkedList<T>();

    private void save(){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            fos = new FileOutputStream(tableName + ".ser");
            oos = new ObjectOutputStream(fos);
            oos.writeObject(rows);
            oos.close();
        }catch(Exception e){
            throw new RuntimeException(e);
        } finally {
            if(oos != null) try {
                oos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(fos != null) try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void load(){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try{
            fis = new FileInputStream(tableName + ".ser");
            ois = new ObjectInputStream(fis);
            this.rows = (List<T>) ois.readObject();
        }catch(FileNotFoundException e){
            this.rows = new LinkedList<T>();
        }catch(Exception e){
            throw new RuntimeException(e);
        }finally{
            if(ois!=null) try {
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(fis!=null) try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    protected Table(String tableName) {
        this.tableName = tableName;
        load();
    }

    protected final String getTableName(){
        return tableName;
    }
    protected final String getId(T row){
        return row.getId();
    }

    protected Object getField(T row, String fieldName){
        try{
            Class<?> c = row.getClass();
            BeanInfo beanInfo = Introspector.getBeanInfo(c);
            for(PropertyDescriptor pd : beanInfo.getPropertyDescriptors()){
                if(pd.getName().equals(fieldName)){
                    return pd.getReadMethod().invoke(row);
                }
            }
            throw new IllegalArgumentException("Unknown field name " + fieldName);
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    private boolean equals(Object obj1, Object obj2){
        if(obj1 == null) return obj2 == null;
        return obj1.equals(obj2);
    }

    public T findById(String id) {
        for(T row : rows){
            if (equals(id, getId(row))) return row;
        }
        return null;
    }

    public List<T> findAll(final String orderBy) {
        List<T> result = new LinkedList<T>(rows);
        Collections.sort(result,new RowComparator(orderBy));
        return rows;
    }

    class RowComparator implements Comparator<T>{
        private String orderBy;
        public RowComparator(String orderBy){
            this.orderBy = orderBy;
        }
        public int compare(T row1, T row2) {
            Object v1 = getField(row1, orderBy);
            Object v2 = getField(row1, orderBy);
            if(v1 != null && v1.equals(v2)) return 0;
            if(v1 instanceof Comparable && v2 instanceof Comparable){
                Comparable c1 = (Comparable) v1;
                Comparable c2 = (Comparable) v2;
                return c1.compareTo(c2);
            }
            if(v1 instanceof Number && v2 instanceof Number){
                double d1 = ((Number) v1).doubleValue();
                double d2 = ((Number) v2).doubleValue();
                if(d1 < d2) return -1;
                if(d1 == d2) return 0;
                return 1;
            }
            throw new IllegalArgumentException("Rows are not comparable by field " + orderBy);
        }
    }

    public List<T> findWhere(String fieldName, Object value, final String orderBy){
        LinkedList<T> result = new LinkedList<T>();
        for(T row : rows){
            if (equals(value, getField(row, fieldName))) result.add(row);
        }
        Collections.sort(result,new RowComparator(orderBy));
        return result;
    }

    public int insert(T row){
        if(findById(getId(row)) != null) throw new DuplicateKeyException("Duplicate key " + getId(row) + " in " + getTableName());
        rows.add(row);
        save();
        return 1;
    }

    public int delete(String id){
        T foundRow = findById(id);
        if(foundRow != null){
            rows.remove(foundRow);
            save();
            return 1;
        }
        return 0;
    }

    public int update(String id, T row){
        T foundRow = findById(id);
        if(foundRow == null) return 0;
        int position = rows.indexOf(foundRow);
        rows.set(position, row);
        save();
        return 1;
    }
}
