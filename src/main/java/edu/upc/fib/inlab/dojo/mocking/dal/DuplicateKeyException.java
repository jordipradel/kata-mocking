package edu.upc.fib.inlab.dojo.mocking.dal;

public class DuplicateKeyException extends RuntimeException {
    public DuplicateKeyException(String msg) {
        super(msg);
    }
}
