package edu.upc.fib.inlab.dojo.mocking.dal;

import java.io.Serializable;
import java.util.UUID;

public class ToDoRow implements Row, Serializable {
    private String id;
    private String title;
    private String description;
    private int priority;

    public ToDoRow() {
    }

    public ToDoRow(String title, String description, int priority) {
        if (title == null) throw new IllegalArgumentException("Title can't be null");
        this.id = UUID.randomUUID().toString();
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    public String getId() {
        return "" + id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "ToDoRow{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDoRow toDoRow = (ToDoRow) o;

        if (priority != toDoRow.priority) return false;
        if (description != null ? !description.equals(toDoRow.description) : toDoRow.description != null) return false;
        if (id != null ? !id.equals(toDoRow.id) : toDoRow.id != null) return false;
        if (!title.equals(toDoRow.title)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + title.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + priority;
        return result;
    }
}
