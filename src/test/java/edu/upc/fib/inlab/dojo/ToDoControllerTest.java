package edu.upc.fib.inlab.dojo;

import edu.upc.fib.inlab.dojo.mocking.Service;
import edu.upc.fib.inlab.dojo.mocking.ServiceLocator;
import edu.upc.fib.inlab.dojo.mocking.domain.ToDo;
import edu.upc.fib.inlab.dojo.mocking.domain.ToDoController;
import edu.upc.fib.inlab.dojo.mocking.domain.ToDoList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class ToDoControllerTest {


    @Test
    public void testGetToDos(){
        ToDoController controller = (ToDoController) ServiceLocator.locate(Service.todoController);
        List<ToDoList> result = controller.getToDoLists();
        assertEquals(1, result.size());
        ToDoList tdd = result.get(0);
        assertEquals("TDD",tdd.getName());
        List<ToDo> todos = tdd.getTodos();
        assertEquals(4,todos.size());
        ToDo todo1 = todos.get(0);
        assertEquals("Write a test",todo1.getTitle());
    }

    @Test
    public void testGetListByName(){
        ToDoController controller = (ToDoController) ServiceLocator.locate(Service.todoController);
        ToDoList result = controller.getToDoList("TDD");
        assertNotNull(result);
        assertEquals("TDD", result.getName());
    }

    @Test
    public void testCreateList(){
        ToDoController controller = (ToDoController) ServiceLocator.locate(Service.todoController);
        controller.newList("Spread the word about the coding dojo");
        ToDoList result = controller.getToDoList("Spread the word about the coding dojo");
        assertNotNull(result);
        assertEquals("Spread the word about the coding dojo",result.getName());
        assertEquals(0,result.getTodos().size());
        assertNotNull(result.getId());
    }

}
